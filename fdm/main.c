//
// Created by Andrey Prokopenko on 21/03/2022.
// Task:
//  Разработать программу решения уравнения струны методом конечных разностей
// Уравнение имеет следующий вид:
//  du/dt = -a*du/dx, где
//  t - время,
//  x - пространственная координата, вдоль которой ориентирована струна,
//  u - функция,
//  a - const,
//

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#include "common/common.h"
#include "consistent/fnd_consist.h"
#include "parallel/fnd_parallel.h"
#include "utils/utils.h"

#define A_K -1.0

double my_u_func(double x) {
  double eps = DBL_EPSILON;
  if ((-2 - eps < x && x < -2 + eps) ||
      (2 + eps < x && x < 2 + eps)) {
    return 0;
  }
  return x < -1 || x > 1 ? 0  : 1;
}

double init_u_func(double x) {
  return sin(M_PI_2 * x);
}

double a_func(double x) {
  return sin(init_u_func(x));
}

double my_external_exp_func(double x, double t) {
  return 0;
  //	x += 1;
  //	t += 1 + t / x - x * t / 10 * 10;
  //	return x / 0.11 / 1.11 + 1.11 / x + 0.111 / 1.111 - (t / x) / 1.111;
}

int main(int argc, char **argv) {
  settings_t settings = {};
  status_t status = get_settings(&settings, argc, argv);
  if (status != OK) {
    return EXIT_FAILURE;
  }

  //! It is GNU extension too. It can be changed to classic declaration like
  //! external_exposure_t_func func = my_function_what_i_want;
  external_exposure_t_func func = my_external_exp_func;

  ///////////////////////////////// Calc parallel way
  ////////////////////////////////////

  grid_t grid_p = {};
  if (!create_grid(&grid_p, settings.n_grid_nodes, settings.modeling_time,
                   &settings)) {
    fprintf(stderr, "Init grid failed\n");
    return EXIT_FAILURE;
  }

  // This is array of start conditions. It can be set by function or manually
  const int N_COND = 1;
  condition_t s_cond[N_COND];
  int counter = 0;
  //    int start = grid_p.size2 / 2 - 4;
  s_cond[counter].i = 0;
  s_cond[counter].condition = 0;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 1;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 2;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 3;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 2;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 1;
  //    s_cond[counter++].i = ++start;
  //    s_cond[counter].condition = 0;

  if (!init_start_conditions(&grid_p, s_cond, N_COND)) {
    fprintf(stderr,
            "WARN: Init start p_conditions failed\n"
            "Set another init conditions or increase number of nodes\n");
  }

  //! It is GNU extension. It can be changed to classic declaration like
  //! double my_function_what_i_want(double);
  //! ...
  //! derivative_cond_t_func d_func = my_function_what_i_want;
  derivative_cond_t_func d_func = init_u_func;
  double a_k = A_K; // 0.9 * grid_p.dx / grid_p.dt;

  printf("%d %d %d\n", settings.n_thread, settings.modeling_time,
         settings.n_grid_nodes);

  if (!condition_approximation(&grid_p, d_func)) {
    fprintf(stderr, "Init start conditions failed\n");
    p_destroy_grid(&grid_p, settings.n_thread);
    return EXIT_FAILURE;
  }

  double t_way2 = 0;
  plot_3d_graphic(grid_p.size2, grid_p.prev, grid_p.dx, X_SHIFT);
  if (settings.n_thread == 1) {
    solve_equation(&grid_p, &t_way2, func, a_k, a_func);
  } else if (!p_solve_equation(&grid_p, &t_way2, func, a_k,
                               settings.n_thread)) {
    fprintf(stderr, "Calculations failed\n");
    p_destroy_grid(&grid_p, settings.n_thread);
    return EXIT_FAILURE;
  }

  print_layer(grid_p.dx, grid_p.size2, grid_p.prev);
  print_layer(grid_p.dx, grid_p.size2, grid_p.current);
  print_layer(grid_p.dx, grid_p.size2, grid_p.old);

  plot_3d_graphic(grid_p.size2, grid_p.prev, grid_p.dx, X_SHIFT);

  p_destroy_grid(&grid_p, settings.n_thread);
  return EXIT_SUCCESS;
}
