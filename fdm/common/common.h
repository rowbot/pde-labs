//
// Created by starman on 13/04/2020.
//

#ifndef LAB2_LAB2_COMMON_COMMON_H_
#define LAB2_LAB2_COMMON_COMMON_H_

#include <stdio.h>
#include <stdbool.h>
#define X_LIMIT 1.0
#define DT 0.01
#define DX 0.1
#define X_SHIFT -2

typedef struct {
    unsigned size2;
    unsigned size1;
    double dx;
    double dt;
    double T;
    double x_limit;
    double *old;
    double *prev;
    double *current;
    FILE* gplt;
} grid_t;

typedef struct {
    size_t i;
    double condition;
} condition_t;

typedef double (*external_exposure_t_func)(double x, double t);

typedef double (*derivative_cond_t_func)(double x);

/// Print grid elements. Line by line
/// \param [in] grid
void print_grid(grid_t *grid);

/// Frees allocated to grid memory
/// \param [in,out] grid
void destroy_grid(grid_t *grid);

#endif  // LAB2_LAB2_COMMON_COMMON_H_
