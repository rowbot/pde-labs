//
// Created by Andrey Prokopenko on 13/04/2020.
//

#include "common.h"
#include <stdio.h>
#include <stdlib.h>

void print_grid(grid_t *grid) {
    for (int i = 0; i < grid->size2; ++i) {
        printf("%lf ", grid->current[i]);
    }
    printf("\n");
    for (int i = 0; i < grid->size2; ++i) {
        printf("%lf ", grid->prev[i]);
    }
    printf("\n");
    for (int i = 0; i < grid->size2; ++i) {
        printf("%lf ", grid->old[i]);
    }
    printf("\n\n");
}

void destroy_grid(grid_t *grid) {
    free(grid->prev);
    free(grid->current);
    free(grid->old);
    fclose(grid->gplt);
    grid->prev = NULL;
    grid->current = NULL;
    grid->old = NULL;
    grid->dt = 0;
    grid->dx = 0;
    grid->size1 = 0;
    grid->size2 = 0;
}
