//
// Created by Andrey Prokopenko on 13/04/2020.
//

#include <float.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/time.h>

#include "common/common.h"
#include "consistent/fnd_consist.h"
#include "utils/utils.h"

bool create_grid(grid_t *grid, unsigned int n_size, int time,
                 settings_t *settings) {
  grid->gplt = fopen("/tmp/lab2.dat", "w+");
  if (!grid->gplt) {
    return false;
  }
  grid->prev = (double *)calloc((size_t)n_size, sizeof(double));
  if (!grid->prev) {
    fclose(grid->gplt);
    return false;
  }
  grid->current = (double *)calloc((size_t)n_size, sizeof(double));
  if (!grid->current) {
    free(grid->prev);
    fclose(grid->gplt);
    return false;
  }
  grid->old = (double *)calloc((size_t)n_size, sizeof(double));
  if (!grid->old) {
    free(grid->prev);
    free(grid->current);
    fclose(grid->gplt);
    return false;
  }

  //    grid->item = (double **)calloc(time, sizeof(double *));
  //    if (!grid->item) {
  //        return false;
  //    }
  //    for (int i = 0; i < time; ++i) {
  //        grid->item[i] = (double *)calloc((size_t)n_size, sizeof(double));
  //        if (!grid->item[i]) {
  //            for (int j = 0; j < i; ++j) {
  //                free(grid->item[j]);
  //            }
  //            return false;
  //        }
  //    }
  grid->dt = DT; //((double)time) / L_LIMIT;
  grid->size1 = settings->t_limit / grid->dt;
  grid->dx = DX; // X_LIMIT / n_size;
  grid->size2 = settings->x_limit / grid->dx;
  return true;
}

bool init_start_conditions(grid_t *grid, condition_t *conditions, int n_cond) {
  if (conditions != NULL && n_cond > 0) {
    for (int i = 0; i < n_cond; ++i) {
      if (grid->size2 < conditions[i].i) {
        return false;
      }
      grid->prev[conditions[i].i] = conditions[i].condition;
      grid->old[conditions[i].i] = conditions[i].condition;
    }
  }
  return true;
}

bool condition_approximation(grid_t *grid, derivative_cond_t_func d_cond) {
  double diff = X_SHIFT;
  if (d_cond != NULL && grid->size1 > 1) {
    for (size_t i = 0; i < grid->size2; ++i) {
      grid->prev[i] = d_cond(grid->dx * (double)i + diff);
    }
    return true;
  }
  return false;
}

bool double_eq(double b, double a) {
  double eps = DBL_EPSILON;
  return a - eps < b && b < a + eps;
}

void solve_equation(grid_t *res, double *time_ptr,
                    external_exposure_t_func func, double a_k,
                    derivative_cond_t_func a_func) {
  struct timeval tv1;
  gettimeofday(&tv1, NULL);
  for (size_t k = 1; k < res->size1 - 1; ++k) {
    for (size_t i = 1; i < res->size2; ++i) {
      a_k = a_func(res->prev[i]);
      double coef = a_k * res->dt / res->dx;
      double diff = a_k < 0 ? res->prev[i + 1] - res->prev[i]
                            : res->prev[i] - res->prev[i - 1];
      res->current[i] = res->prev[i] - coef * diff;
    }
    res->current[0] = 0;
    res->current[res->size2 - 1] = 0;
    double *tmp = res->old;
    res->old = res->prev;
    res->prev = res->current;
    res->current = tmp;
  }
  struct timeval tv2;
  gettimeofday(&tv2, NULL);
  if (time_ptr) {
    *time_ptr = (double)(tv2.tv_usec - tv1.tv_usec) +
                (double)(tv2.tv_sec - tv1.tv_sec) * 100000;
  }
}
