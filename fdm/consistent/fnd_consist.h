//
// Created by Andrey Prokopenko on 13/04/2020.
//

#ifndef LAB2_LAB2_CONSISTENT_FND_CONSIST_H_
#define LAB2_LAB2_CONSISTENT_FND_CONSIST_H_

#include <stdbool.h>
#include "common/common.h"
#include "utils/utils.h"

/// Initializes grid structure using first derivative condition
/// \param [in,out] grid - grid
/// \param [in] d_cond - first derivative [u_0(x_i)]
/// \return result of operation
bool condition_approximation(grid_t *grid, derivative_cond_t_func d_cond);

/// Solves wave equation using finite difference method (FDM) and initializes grid by
/// solutions.
/// \param [in,out] res - initialized by start conditions grid
/// \param [out] time_ptr - memory where will be written all time of calculations
/// \param [in] func - external exposure [f(x, t)]
/// \param [in] a_k - coefficient in wave equation
void solve_equation(grid_t *res, double *time_ptr,
                    external_exposure_t_func func, double a_k,
                    derivative_cond_t_func a_func);

/// Initializes first level of grid by start conditions
/// \param [in, out] grid - grid which might be initialized
/// \param [in] conditions - array of start conditions
/// \param [in] n_cond - number of start conditions
/// \return result of operation
bool init_start_conditions(grid_t *grid, condition_t *conditions, int n_cond);

/// Allocates memory for grid elements by n_size and initialize several fields there
/// \param [out] grid - empty object that might be initialized
/// \param [in] n_size - number of elements in grid level
/// \param [in] time - time for calculate dt
/// \return result of operation
bool create_grid(grid_t *grid, unsigned int n_size, int time,
                 settings_t *settings);

#endif  // LAB2_LAB2_CONSISTENT_FND_CONSIST_H_
