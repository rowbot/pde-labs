//
// Created by Andrey Prokopenko on 14/04/2020.
//

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "parallel/fnd_parallel.h"

static bool g_has_error = false;

static p_barrier_t barrier = {0, 0, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER};

void inc_barrier_counter() {
    pthread_mutex_lock(&barrier.mutex);
    ++barrier.counter;
    pthread_cond_signal(&barrier.cond_var);
    pthread_mutex_unlock(&barrier.mutex);
}

void set_counter(int n) {
    pthread_mutex_lock(&barrier.mutex);
    barrier.max = n;
    pthread_mutex_unlock(&barrier.mutex);
}

void reset_counter() {
    pthread_mutex_lock(&barrier.mutex);
    barrier.counter = 0;
    pthread_mutex_unlock(&barrier.mutex);
}

bool p_run_in_thread_common(unsigned x_part, int n_thread, routine_t_func thread_func,
                            arg_data_t *template, int size) {
    static pthread_barrier_t barrier;
    // if number of threads is not 0 then create barrier for wait end of each
    if (n_thread != 0) {
        pthread_barrier_init(&barrier, NULL, n_thread);
    }

    // create array of objects that will be passed to the thread function
    arg_data_t *data = malloc(sizeof(arg_data_t) * n_thread);
    if (!data) {
        fprintf(stderr, "Allocation failed\n");
        return false;
    }

    // thread generation loop
    int max_idx = n_thread - 1;
    for (unsigned i = 0; i < max_idx; ++i) {
        pthread_t thread = 0;             // thread identifier
        unsigned end = (i + 1) * x_part;  // index of last item in array part for thread
        if (template) {
            data[i] = *template;
        }
        data[i].s = i * x_part;
        data[i].e = end;
        data[i].barrier = &barrier;

        union pthread_attr_t attr;
        pthread_attr_init(&attr);

        pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        // create and start POSIX thread
        int err_flag = pthread_create(&thread, &attr, thread_func, &(data[i]));
        if (err_flag != 0) {  // check if pthread_create() call was succeed
            fprintf(stderr, "pthread_create failed\n");
            free(data);
            return false;
        }
    }
    if (template) {
        data[max_idx] = *template;
    }
    data[max_idx].s = (max_idx)*x_part;
    data[max_idx].e = size;
    data[max_idx].barrier = &barrier;

    thread_func((void *)&(data[max_idx]));
    pthread_barrier_destroy(&barrier);
    if (g_has_error) {
        fprintf(stderr, "g_has_error occurred\n");
        free(data);
        return false;
    }

    return true;
}

void p_destroy_grid(grid_t *grid, int n_threads) {
//    free(grid->prev);
//    free(grid->current);
//    free(grid->old);
    fclose(grid->gplt);
    grid->prev = NULL;
    grid->current = NULL;
    grid->old = NULL;
    grid->dt = 0;
    grid->dx = 0;
    grid->size1 = 0;
    grid->size2 = 0;
}

void *p_calc_grid_items(void *arg) {
    // parse arg object
    arg_data_t *data = (arg_data_t *)arg;
    //    int k = data->k;
    double a_k = data->a_k;
    grid_t *grid_ptr = data->grid_ptr;
    double coef = a_k * grid_ptr->dt / grid_ptr->dx;
    // create calculations for nodes between s position and e
    for (size_t k = 1; k < grid_ptr->size1 - 1; ++k) {
        for (int i = data->s; i < data->e; ++i) {
            if (grid_ptr->size2 < i) {
                return arg;
            }
            double diff = a_k < 0 ? grid_ptr->prev[i + 1] - grid_ptr->prev[i]
                                  : grid_ptr->prev[i] - grid_ptr->prev[i - 1];
            grid_ptr->current[i] = grid_ptr->prev[i] - coef * diff;
            if (grid_ptr->size2 < i) {
                return arg;
            }
        }
        inc_barrier_counter();
        while (barrier.counter < barrier.max) {
            pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
            pthread_mutex_lock(&lock);
            pthread_cond_wait(&barrier.cond_var, &lock);
        }
        if (data->s == 0) {
	        double *tmp = grid_ptr->old;
	        grid_ptr->old = grid_ptr->prev;
	        grid_ptr->prev = grid_ptr->current;
	        grid_ptr->current = tmp;
        }
    }

    //    inc_barrier_counter();
    return NULL;
}

bool run_in_thread(arg_data_t *data, pthread_t *thread_ids, unsigned x_part, int n_threads,
                   routine_t_func thread_func, arg_data_t *template, int size) {
    int max_idx = n_threads - 1;
    union pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
    // I'd like to do threads detached but I have some problems with SIGSEGV
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for (unsigned i = 0; i < max_idx; ++i) {  // thread generation loop
        if (template) {
            memcpy(&(data[i]), template, sizeof(*template));
        }
        data[i].s = i * x_part + 1;  // create shift of our thread window
        data[i].e = (i + 1) * x_part + 1;
        // create and start POSIX thread
        int err_flag = pthread_create(&(thread_ids[i]), &attr, thread_func, &(data[i]));
        if (err_flag != 0) {  // check if call of pthread_create() is succeed
            fprintf(stderr, "pthread_create failed\n");
            return false;
        }
    }

    // handle the end of data
    if (template) {
        memcpy(&(data[max_idx]), template, sizeof(*template));
    }
    data[max_idx].s = (max_idx)*x_part + 1;
    data[max_idx].e = size;

    thread_func((void *)&(data[max_idx]));
    if (g_has_error) {
        fprintf(stderr, "g_has_error occurred\n");
        return false;
    }

    return true;
}

bool p_solve_wave_equation(grid_t *grid, double *time_ptr, external_exposure_t_func func, double a_k,
                           unsigned n_threads) {
    // create array of objects that will be passed to the thread function
    arg_data_t *data = malloc(sizeof(arg_data_t) * n_threads);
    if (!data) {
        fprintf(stderr, "Allocation failed\n");
        return false;
    }
    // create array of thread ids that will be created in the run_in_thread function
    pthread_t *thread_ids = malloc(sizeof(pthread_t) * n_threads);
    if (!thread_ids) {
        fprintf(stderr, "Allocation failed\n");
        free(data);
        return false;
    }
    // init of arg_data template
    arg_data_t data2 = {};
    data2.grid_ptr = grid;
    data2.a_k = a_k;
    data2.func = func;
    int x_part = grid->size2 / n_threads;
    struct timeval tv3, tv4;
    gettimeofday(&tv3, NULL);
    for (int k = 1; k < (int)grid->size1 - 1; ++k) {
        data2.k = k;
        bool res =
            run_in_thread(data, thread_ids, x_part, n_threads, p_calc_grid_items, &data2, grid->size2);
        while (barrier.counter < n_threads) {
            pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
            pthread_mutex_lock(&lock);
            pthread_cond_wait(&barrier.cond_var, &lock);
        }
        // Wait for the end of each thread. It's necessary, because without it (and with
        // detached threads) sometimes data free too early, and I have SIGSEGV error.
        // Occasionally program works ok, but it is not a good result, I suppose. I think
        // the problem is in my barrier realization
        for (int j = 0; j < n_threads; ++j) {
            pthread_join(thread_ids[j], NULL);
        }
        if (!res) {
            free(data);
            free(thread_ids);
            return false;
        }
        //        for (int i = 0; i < grid->size2; ++i) {
        //            fprintf(grid->gplt, "%lf %lf ", grid->dx * i, grid->current[i]);
        //        }
        //        fprintf(grid->gplt, "\n");
        double *tmp = grid->old;
        grid->old = grid->prev;
        grid->prev = grid->current;
        grid->current = tmp;
    }
    // calc execution time
    gettimeofday(&tv4, NULL);
    if (time_ptr) {
        *time_ptr = (double)(tv4.tv_usec - tv3.tv_usec) + (double)(tv4.tv_sec - tv3.tv_sec) * 100000;
    }

    free(data);
    free(thread_ids);
    return true;
}

bool p_solve_equation(grid_t *grid, double *time_ptr, external_exposure_t_func func, double a_k,
                              unsigned n_threads) {
    // create array of objects that will be passed to the thread function
    arg_data_t *data = malloc(sizeof(arg_data_t) * n_threads);
    if (!data) {
        fprintf(stderr, "Allocation failed\n");
        return false;
    }
    // create array of thread ids that will be created in the run_in_thread function
    pthread_t *thread_ids = malloc(sizeof(pthread_t) * n_threads);
    if (!thread_ids) {
        fprintf(stderr, "Allocation failed\n");
        free(data);
        return false;
    }
    // init of arg_data template
    arg_data_t data2 = {};
    data2.grid_ptr = grid;
    data2.a_k = a_k;
    data2.func = func;
    int x_part = grid->size2 / n_threads;
    struct timeval tv3, tv4;
    gettimeofday(&tv3, NULL);
    barrier.max = n_threads;
    int max_idx = n_threads - 1;
    union pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
    // I'd like to do threads detached but I have problem with SIGSEGV
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for (int i = 0; i < max_idx; ++i) {  // thread generation loop
            memcpy(&(data[i]), &data2, sizeof(data2));
            data[i].s = i * x_part + 1;  // create shift of our thread window
            data[i].e = (i + 1) * x_part + 1;
            // create and start POSIX thread
            int err_flag = pthread_create(&(thread_ids[i]), &attr, p_calc_grid_items, &(data[i]));
            if (err_flag != 0) {  // check if call of pthread_create() is succeed
                    fprintf(stderr, "pthread_create failed\n");
                    return false;
            }
    }

    // handle the end of data
    memcpy(&(data[max_idx]), &data2, sizeof(data2));
    data[max_idx].s = (max_idx)*x_part + 1;
    data[max_idx].e = grid->size2;

    p_calc_grid_items((void *)&(data[max_idx]));
    gettimeofday(&tv4, NULL);
    if (time_ptr) {
        *time_ptr = (double)(tv4.tv_usec - tv3.tv_usec) + (double)(tv4.tv_sec - tv3.tv_sec) * 100000;
    }

    free(data);
    free(thread_ids);
    return true;
}