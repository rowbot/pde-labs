//
// Created by Andrey Prokopenko on 14/04/2020.
//

#ifndef LAB2_LAB2_PARALLEL_FND_PARALLEL_H_
#define LAB2_LAB2_PARALLEL_FND_PARALLEL_H_

#include <pthread.h>

#include "common/common.h"

typedef struct {
    int s;
    int e;
    int k;
    int n_cond;
    double a_k;
    external_exposure_t_func func;
    derivative_cond_t_func cond_func;
    condition_t *cond_array;
    grid_t *grid_ptr;
    pthread_barrier_t *barrier;
} arg_data_t;

typedef struct {
    int counter;
    int max;
    pthread_mutex_t mutex;
    pthread_cond_t cond_var;
} p_barrier_t;

typedef void *(*routine_t_func)(void *);

/// Increments barrier counter
void inc_barrier_counter();

/// Sets n value as barrier counter
/// \param n
void set_counter(int n);

/// Reset barrier counter value
void reset_counter();

/// Allocates memory for grid elements by n_size and initialize several fields there
/// \param [out] grid - empty object that might be initialized
/// \param [in] n_size - number of elements in grid level
/// \param [in] time - time for calculate dt
/// \param [in] n_thread - number of threads
/// \return result of operation
bool p_create_grid(grid_t *grid, unsigned int n_size, int time, int n_thread);

/// Initializes first level of grid by start conditions
/// \param [in, out] grid - grid which might be initialized
/// \param [in] conditions - array of start conditions
/// \param [in] n_cond - number of start conditions
/// \param [in] n_thread - number of threads
/// \return result of operation
bool p_init_start_conditions(grid_t *grid, condition_t *conditions, int n_cond,
                             int n_threads);

/// Frees allocated for grid elements memory
/// \param [in, out] grid - grid which might be destroyed
/// \param [in] n_thread - number of threads
void p_destroy_grid(grid_t *grid, int n_threads);

/// Runs in threads thread_func
/// \param [in] x_part - width of x buffer for each thread
/// \param [in] n_threads - number of threads
/// \param [in] thread_func - routine func
/// \param [in] template - arg_data_t struct with several fields which filled
/// \param [in] size - number of nodes
/// \return result of operation
bool p_run_in_thread_common(unsigned x_part, int n_thread, routine_t_func thread_func,
                            arg_data_t *template, int size) __attribute__((deprecated));

/// Allocate memory to grid elements which described in the arg in thread
/// \param arg - arg = (void*) (arg_data_t)
/// \return NULL if success
void *th_allocate(void *arg);

/// Initializes first level of grid by start conditions in thread
/// \param [in] arg
/// \return NULL if success
void *th_init_start_cond(void *arg);

/// Initializes grid structure using first derivative condition in thread
/// \param [in] arg
/// \return NULL if success
void *th_approximation(void *arg);

/// Solves wave equation using finite difference methd (FDM) and initializes grid
/// elements by solutions which described in the arg
/// \param [in] arg - arg = (void*) (arg_data_t)
/// \return NULL if success
void *p_calc_grid_items(void *arg);

/// Run in threads void *th_approximation(void *arg) function
/// \param grid - grid that might be initialized
/// \param d_cond - first derivative [u_0(x_i)]
/// \param a_k - coefficient in wave equation
/// \param n_threads - number of threads
/// \return result of operation
bool p_condition_approximation(grid_t *grid, derivative_cond_t_func d_cond, double a_k,
                               int n_threads);

/// Run in threads void *p_calc_grid_items(void *arg) function and count all time of
/// calculations
/// \param [in,out] grid - grid which items might be calculated
/// \param [out] time_ptr - memory where will be written all time of calculations
/// \param [in] func - external exposure
/// \param [in] a_k - coefficient in wave equation
/// \param [in] n_threads - number of threads
/// \return result of operation
bool p_solve_wave_equation(grid_t *grid, double *time_ptr, external_exposure_t_func func,
                           double a_k, unsigned n_threads);

/// Run in threads void *p_calc_grid_items(void *arg) function and count all time of
/// calculations
/// \param [in,out] grid - grid which items might be calculated
/// \param [out] time_ptr - memory where will be written all time of calculations
/// \param [in] func - external exposure
/// \param [in] a_k - coefficient in wave equation
/// \param [in] n_threads - number of threads
/// \return result of operation
bool p_solve_equation(grid_t *grid, double *time_ptr, external_exposure_t_func func,
                           double a_k, unsigned n_threads);

/// Runs in threads thread_func
/// \param [in] data - allocated memory for data which will be used in threads
/// \param [in] x_part - width of x buffer for each thread
/// \param n_threads - number of threads
/// \param thread_func - routine func
/// \param template - arg_data_t struct with several fields which filled
/// \param size - number of nodes
/// \return result of operation
bool run_in_thread(arg_data_t *data, pthread_t *thread_ids, unsigned x_part, int n_threads,
                   routine_t_func thread_func, arg_data_t *template, int size);

#endif  // LAB2_LAB2_PARALLEL_FND_PARALLEL_H_
