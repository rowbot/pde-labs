//
// Created by Andrey Prokopenko on 12/04/2020.
//

#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "utils/utils.h"

int convert_to_int(char *arg) {
    char *end = NULL;
    int n_thread = (int)strtol(arg, &end, 10);
    // Check for various possible errors
    if ((errno == ERANGE && (n_thread == INT_MAX || n_thread == INT_MIN)) ||
        (errno != 0 && n_thread == 0)) {
        return -1;
    }

    if (end == optarg) {
        fprintf(stderr, "No digits were found\n");
        return -1;
    }
    return n_thread;
}

double convert_to_double(char *arg) {
  char *end = NULL;
  double val = strtod(arg, &end);
  // Check for various possible errors
  if ((errno == ERANGE) || (errno != 0)) {
    return -1;
  }

  if (end == optarg) {
    fprintf(stderr, "No digits were found\n");
    return -1;
  }
  return val;
}

void usage_details() {
    printf(
        "Usage: lab-fdm [OPTION] [OPTION] [OPTION]\n"
        "Example: lab-fdm -t 4 -s 10 -n 200\n"
        "------- Listing options -------\n"
        "-t threads        Number of threads\n"
        "-s seconds        Set program executable time\n"
        "-n nodes          Nodes of calculus grid. IMPORTANT! [nodes mod 8] HAVE TO be "
        "equal 0\n");
}

status_t get_settings(settings_t *settings, int argc, char **argv) {
    settings_t default_settings = {};
    status_t status = OK;

    if (argc < 7) {
        status = ERROR;
    }

    int opt = getopt(argc, argv, OPT_KEYS);
    while (opt != -1 && status != ERROR) {
        if (!optarg) {
            status = ERROR;
            continue;
        }

        switch (opt) {
            case 't': {
                default_settings.n_thread = convert_to_int(optarg);
                if (default_settings.n_thread == INT_MAX) {
                    status = ERROR;
                }
                break;
            }

            case 'n': {
                default_settings.n_grid_nodes = convert_to_int(optarg);
                if (default_settings.n_grid_nodes == INT_MAX || default_settings.n_grid_nodes % 8 != 0) {
                    status = ERROR;
                }
                break;
            }

            case 's': {
                default_settings.modeling_time = convert_to_int(optarg);
                if (default_settings.n_thread == INT_MAX) {
                    status = ERROR;
                }
                break;
            }
            case 'T': {
              default_settings.t_limit = convert_to_double(optarg);
              break;
            }
            case 'H': {
              default_settings.x_limit = convert_to_double(optarg);
              break;
            }

            case 'h':
            case '?': {
                status = UNKNOWN_FLAG;
                usage_details();
                break;
            }

            default: {
                status = UNKNOWN_FLAG;
                //      usage_details();
                break;
            }
        }
        opt = getopt(argc, argv, OPT_KEYS);
    }

    if (status == ERROR) {
        usage_details();
    }
    *settings = default_settings;
    return status;
}

/// Write input data to /tmp/data.tmp file and then create pipeline with gnuplot.
/// Opens an interface that one can use to send commands as if they were
/// typing into the gnuplot command line. "The -persistent" keeps the plot
/// open even after your C program terminates.
/// \param n_commands
/// \param script
/// \param n_points
/// \param points_x
/// \param points_y1
/// \param points_y2
void plot_graphic(int n_commands, char **script, int n_points, double *points_x, double *points_y1,
                  double *points_y2) {
    FILE *temp = fopen("/tmp/data.tmp", "w");
    for (int i = 0; i < n_points; ++i) {  // Write the data to a temporary file
        fprintf(temp, "%lf %lf %lf \n", points_x[i], points_y1[i], points_y2[i]);
    }
    fclose(temp);

    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    for (int i = 0; i < n_commands; ++i) {  // Send commands to gnuplot one by one
        fprintf(gnuplot_pipe, "%s \n", script[i]);
    }
    fflush(gnuplot_pipe);
    pclose(gnuplot_pipe);
}

/// Write input data to /tmp/data.tmp file and then create pipeline with gnuplot.
/// Opens an interface that one can use to send commands as if they were
/// typing into the gnuplot command line. "The -persistent" keeps the plot
/// open even after your C program terminates.
/// \param dx
/// \param n_points
/// \param points_u
/// \param shift
void plot_3d_graphic(int n_points, double *points_u, double dx, double shift) {
    FILE *temp = fopen("/tmp/data.tmp", "w");
    if (!temp) {
        return;
    }
    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    if (!gnuplot_pipe) {
        fclose(temp);
        return;
    }
    fprintf(gnuplot_pipe, "$map2 << EOD\n");
    for (int j = 0; j < n_points; ++j) {  // Write the data to a temporary file
        double x_val = dx * j + shift;
        fprintf(temp, "%lf %lf\n", x_val, points_u[j]);
        fprintf(gnuplot_pipe, "%lf %lf\n", x_val, points_u[j]);
    }
    fclose(temp);

    fprintf(gnuplot_pipe, "EOD \n");
    fprintf(gnuplot_pipe, "plot '$map2'\n");

    fflush(gnuplot_pipe);
    pclose(gnuplot_pipe);
}

void draw_animation(int n_commands, char **script, int n_points, double *points_x, int n_z_points,
                    double **points_y) {
    FILE *temp = fopen("/tmp/data.tmp", "w");

    for (int i = 0; i < n_points; ++i) {
        for (int j = 0; j < n_z_points; ++j) {
            fprintf(temp, "%lf %lf ", points_x[i], points_y[j][i]);
        }
        fprintf(temp, "\n");
    }
    fclose(temp);

    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    for (int i = 0; i < n_commands; ++i) {  // Send commands to gnuplot one by one
        fprintf(gnuplot_pipe, "%s \n", script[i]);
    }
    fflush(gnuplot_pipe);
    pclose(gnuplot_pipe);
}

void print_layer(double dx, int n_points, double *points_u) {
  for (int j = 0; j < n_points; ++j) {  // Write the data to a temporary file
    printf("%lf %lf\n", dx * j, points_u[j]);
  }
}
