# Лабораторная работа 1  
Выполнена: Прокопенко Андрей, МСМТ211

## Требования: 
1. cmake 3.2+
2. make 4+
3. gcc 5+
4. gnuplot 5.2+

## Запуск  
```bash
$ mkdir build && cd build
$ cmake .. && make -j 4
$ ./lab-fdm -t <number of threads> -s <time of calculus> -n <number of grid nodes>  
```  
Для справки:  
```bash
$ ./lab2 -h  
```  
По заврешению работы программы:  
```shell script
$ cd .. 
$ rm -r build
```


